import { ReactNode, createContext, useEffect, useReducer, useState } from "react";
import { Cycle, cyclesReducer } from "../reducers/cycles/reducer";
import { addNewCycleAction, finishCurrentCycleAction, interruptCurrentCycleAction } from "../reducers/cycles/actions";
import { differenceInSeconds } from "date-fns";


interface CreateCycleData {
  task: string;
  minutesAmount: number;
}

interface CyclesContextType {
  cycles: Cycle[]
  activeCycle: Cycle | undefined;
  activeCycleId: string | null;
  pastSecondsAmount: number;
  finishCurrentCycle: () => void;
  setPastSeconds: (seconds: number) => void;
  createNewCycle: (data: CreateCycleData) => void;
  interruptCurrentCycle: () => void;
}

interface CyclesContextProviderProps {
  children: ReactNode;
}


export const CyclesContext = createContext({} as CyclesContextType);

export function CyclesContextProvider({ children }: CyclesContextProviderProps) {

  const [cyclesState, dispatch] = useReducer(cyclesReducer, {
    cycles: [],
    activeCycleId: null
  }, (initialState) => {
    const storedState = localStorage.getItem('@timer:cycles-state-1.0.0');
    if (storedState) {
      return JSON.parse(storedState);
    }
    return initialState;
  }); 
  //const [cycles, setCycles] = useState<Cycle[]>([]);
  //const [activeCycleId, setActiveCycleId] = useState<number>(0);

  const { cycles, activeCycleId } = cyclesState;
  const activeCycle = cycles.find(cycle => cycle.id === activeCycleId);

  const [pastSecondsAmount, setPastSecondsAmount] = useState<number>(() => {
    if (activeCycle) {
      return differenceInSeconds(new Date(), new Date(activeCycle.startDate));
    }
    return 0;
  });

  useEffect(() => {
    const stateJson = JSON.stringify(cyclesState);
    localStorage.setItem('@timer:cycles-state-1.0.0', stateJson);
  }, [cyclesState]);

  function setPastSeconds(seconds: number) {
    setPastSecondsAmount(seconds);
  }

  function createNewCycle(data: CreateCycleData) {
    const newCycle: Cycle = {
      id: String(new Date().getTime()),
      task: data.task,
      minutesAmount: data.minutesAmount,
      startDate: new Date()
    };
    
    dispatch(addNewCycleAction(newCycle));

    /*
    setCycles(state => [...state, newCycle]);
    setActiveCycleId(newCycle.id);
    */
    setPastSecondsAmount(0);
  }

  function finishCurrentCycle() {

    dispatch(finishCurrentCycleAction())

    /*
    setCycles(state =>
      state.map(cycle => {
        if (cycle === activeCycle) {
          return { ...cycle, finishedDate: new Date() };
        }
        return cycle;
      })
    );
    */
  }

  function interruptCurrentCycle() {
    dispatch(interruptCurrentCycleAction());
    /*
    setCycles(state =>
      state.map(cycle => {
        if (cycle === activeCycle) {
          return { ...cycle, interruptedDate: new Date() };
        }
        return cycle;
      })
    );
    setActiveCycleId(null);
    */
  }

    return (
        <CyclesContext.Provider 
          value={
            {
                cycles,
                activeCycle,
                activeCycleId,
                pastSecondsAmount,
                finishCurrentCycle,
                setPastSeconds,
                createNewCycle,
                interruptCurrentCycle
            }
          }>
            {children}
        </CyclesContext.Provider>
    );
}